package de.baierfamily.komoot;

import lombok.extern.java.Log;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.math.BigDecimal.valueOf;
import static java.math.RoundingMode.DOWN;

@Component
@Log
public class KomootStatisticsDriver {
    String fileName = "src/main/resources/inputDataAndreas.txt";
    SimpleDateFormat sdtF = new SimpleDateFormat("dd mmm yyyy", Locale.GERMANY);

    private static final Map<BigDecimal, BigDecimal> kmFiboMap = StaticsFactory.createKMFiboMap();
    private static final Map<BigDecimal, BigDecimal> hmFiboMap = StaticsFactory.createHMFiboMap();
    private static final BigDecimal TEN = new BigDecimal(10);
    private static final BigDecimal HUNDRED = new BigDecimal(100);



    @Bean
    public void calculatePerformance() throws IOException {
        String lines = Files.readString(Paths.get(fileName),
                StandardCharsets.UTF_8);
        String[] tourstrings = lines.split("Löschen");
        Stream<String> tourStream = Arrays.stream(tourstrings);
        List<Tour> tours = tourStream
                .map(KomootStatisticsDriver::cleanupPrefix)
                .map(KomootStatisticsDriver::createTour)
        .collect(Collectors.toList());

        List<Tour> perfTours = tours.stream().map(KomootStatisticsDriver::calculatePerformance)
                .collect(Collectors.toList());
        perfTours.forEach((tour) -> log.info(tour.toString()));
        toCSV(perfTours);
    }

    private void toCSV(List<Tour> perfTours) throws IOException {
        Path out = Paths.get("performance.csv");
        List<String> csvStrings = perfTours.stream()
                .map(Tour::toCSVLine)
                .collect(Collectors.toList());
        Files.write(out,csvStrings, Charset.defaultCharset());

    }

    private static String cleanupPrefix(String tourString) {
        boolean replaced=true;
        while(replaced==true){
            replaced=false;
            if(tourString.startsWith("\r")){
                tourString= tourString.substring(2);
                replaced=true;
            }
            if(tourString.startsWith("\n")){
                tourString= tourString.substring(2);
                replaced=true;
            }
        }

        return tourString;
    }

    private static Tour calculatePerformance(Tour tour){
        try {
            BigDecimal kmNormalized = tour.getKmDistance().divide(TEN, DOWN).setScale(0, DOWN).multiply(TEN);
            BigDecimal hmNormalized = getNormalizedHMValue(tour.getHighMeter());
            BigDecimal kmPerf = kmFiboMap.get(kmNormalized).add(tour.getKmDistance());
            BigDecimal hmPerf = hmFiboMap.get(hmNormalized).add(tour.getHighMeter());
            BigDecimal minutes = valueOf(tour.getRideDuration().toMinutes());
            BigDecimal performance = hmPerf.multiply(kmPerf).divide(minutes, DOWN);
            tour.setPerformance(performance);
        }catch(Exception e){
            log.info(e.toString());
        }
        return tour;
    }

    private static BigDecimal getNormalizedHMValue(BigDecimal hm) {
        BigDecimal normKey = valueOf(300);
        ArrayList<BigDecimal> hmKeys = new ArrayList<>(hmFiboMap.keySet());
        Collections.sort(hmKeys);
        for (BigDecimal currentKey : hmKeys) {
            if(currentKey.max(hm).equals(currentKey)){
                break;
            }
            normKey=currentKey;
        }
        return normKey;
    }


    private static Tour createTour(String tourString) {
        System.out.println(tourString);
        String[] split = tourString.split("\n");
        String rideDurationString = split[1];
        String[] rideDurationSplit = rideDurationString.split(":");
        String kmDistance = split[2].split(" ")[0];
        String avgSpeed = split[3].split(" ")[0];
        String highMeter = removeTrailing_m(split[4]);
        String tourdate="";
        if(split.length==7){
            tourdate = removeTrailingBackslashR(split[6]);
        }

        long hours = toLong(rideDurationSplit[0]);
        long minutesToAdd = toLong(rideDurationSplit[1]);
        Duration duration = Duration.ofHours(hours);
        Duration rideDuration = duration.plusMinutes(minutesToAdd);
        return Tour.builder()
                .name(removeTrailingBackslashR(split[0]))
                .rideDuration(rideDuration)
                .kmDistance(toBigDecimal(kmDistance))
                .avgSpeed(toBigDecimal(avgSpeed))
                .highMeter(toBigDecimal(highMeter))
                .rideDate(tourdate)
                .build();
    }

    private static String removeTrailing_m(String s) {
        return s.replace(" m", "");
    }


    private static String removeTrailingDot(String s) {
        return s.replace(".", "");
    }

    private static String removeKMHSuffix(String kmhString) {
        return kmhString.split(" ")[0];
    }

    private static BigDecimal toBigDecimal(String kmDistance) {
        String kmReady = kmDistance.replace(',', '.');
        kmReady =kmReady.replace(" ", "");
        kmReady=removeTrailingBackslashR(kmReady);
        return new BigDecimal(kmReady);
    }

    private static long toLong(String s) {
        String s1 = trimLeadingZero(s);
        String readyString= removeTrailingBackslashR(s1);
        return Long.parseLong(readyString);

    }

    private static String removeTrailingBackslashR(String s1) {
        return s1.replace("\r", "");
    }


    private static String trimLeadingZero(String s) {
        if(s.charAt(0)=='0'){
            s = s.substring(1);
        }
        return s;
    }

}
