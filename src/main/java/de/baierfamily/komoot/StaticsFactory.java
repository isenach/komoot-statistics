package de.baierfamily.komoot;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import static java.math.BigDecimal.valueOf;

class StaticsFactory {

    private static final Map<BigDecimal, BigDecimal> kmFiboMap = new HashMap<>();
    private static final Map<BigDecimal, BigDecimal> hmFiboMap = new HashMap<>();


    static Map<BigDecimal, BigDecimal> createKMFiboMap() {
        kmFiboMap.put(valueOf(0), valueOf(1));
        kmFiboMap.put(valueOf(10),valueOf(2));
        kmFiboMap.put(valueOf(20),valueOf(3));
        kmFiboMap.put(valueOf(30),valueOf(5));
        kmFiboMap.put(valueOf(40),valueOf(8));
        kmFiboMap.put(valueOf(50),valueOf(13));
        kmFiboMap.put(valueOf(60),valueOf(21));
        kmFiboMap.put(valueOf(70),valueOf(34));
        kmFiboMap.put(valueOf(80),valueOf(55));
        kmFiboMap.put(valueOf(90),valueOf(89));
        kmFiboMap.put(valueOf(100),valueOf(144));
        return kmFiboMap;
    }

     static Map<BigDecimal, BigDecimal> createHMFiboMap() {
        hmFiboMap.put(valueOf(0), valueOf(1));
        hmFiboMap.put(valueOf(300), valueOf(2));
        hmFiboMap.put(valueOf(600), valueOf(3));
        hmFiboMap.put(valueOf(900), valueOf(5));
        hmFiboMap.put(valueOf(1200),valueOf(8));
        hmFiboMap.put(valueOf(1500),valueOf(13));
        hmFiboMap.put(valueOf(1800),valueOf(21));
        hmFiboMap.put(valueOf(2100),valueOf(34));
        hmFiboMap.put(valueOf(2400),valueOf(55));
        hmFiboMap.put(valueOf(2700),valueOf(89));
        hmFiboMap.put(valueOf(3000),valueOf(144));
        return hmFiboMap;
    }

}
