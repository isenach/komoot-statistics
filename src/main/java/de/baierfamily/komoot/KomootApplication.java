package de.baierfamily.komoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KomootApplication {

    public static void main(String[] args) {
        SpringApplication.run(KomootApplication.class, args);
    }

}
