package de.baierfamily.komoot;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.Duration;

@Builder
@Getter
@Setter
@ToString
public class Tour {
    public static final String SC = ";";

    private String name;
    private BigDecimal avgSpeed;
    private BigDecimal kmDistance;
    private BigDecimal highMeter;
    private Duration rideDuration;
    private String rideDate;
    private BigDecimal performance;


    public String toCSVLine() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        sb.append(SC);
        sb.append(rideDate);
        sb.append(SC);
        sb.append(replaceDotWithComma(kmDistance.toString()));
        sb.append(SC);
        sb.append(replaceDotWithComma(highMeter.toString()));
        sb.append(SC);
        String durationString = String.format("%d:%02d", rideDuration.toHours(), rideDuration.toMinutesPart());
        sb.append(durationString);
        sb.append(SC);
        sb.append(replaceDotWithComma(performance.toString()));

        return sb.toString();

    }

    private static String replaceDotWithComma(String s) {
        return s.replace(".", ",");
    }


}
